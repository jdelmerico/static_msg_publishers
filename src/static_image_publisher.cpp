#include "ros/ros.h"
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <opencv2/opencv.hpp>

// Publishes image from argv[1] w/ frame_id from argv[2]

int main(int argc, char **argv)
{
	ros::init(argc, argv, "static_image_publisher");
	ros::NodeHandle n;
	ros::Publisher im_pub = n.advertise<sensor_msgs::Image>("image", 100);
	ros::Rate loop_rate(10);

	// Load image from argv[1] into OpenCV Mat
	cv::Mat im = cv::imread(argv[1],1);	

	// Convert to ROS msg
	cv_bridge::CvImage cv_ptr;
	cv_ptr.image = im;
	cv_ptr.encoding = sensor_msgs::image_encodings::RGB8;
	sensor_msgs::ImagePtr msg = cv_ptr.toImageMsg();
	msg->header.frame_id = argv[2];

	// Publish
	while (ros::ok())
	{
		msg->header.stamp = ros::Time::now();
		im_pub.publish(msg);
		ros::spinOnce();
		loop_rate.sleep();
	}	
	return 0;
}
