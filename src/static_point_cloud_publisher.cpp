#include "ros/ros.h"
#include "sensor_msgs/PointCloud2.h"
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

// Publishes pcd from argv[1] w/ frame_id from argv[2]

int main(int argc, char** argv)
{
	ros::init(argc,argv, "static_point_cloud_publisher");
	ros::NodeHandle n;
	ros::Publisher pc_pub = n.advertise<sensor_msgs::PointCloud2>("point_cloud",100);
	ros::Rate loop_rate(10);

	pcl::PointCloud<pcl::PointXYZ> cloud;
	if (pcl::io::loadPCDFile<pcl::PointXYZ> (argv[1], cloud) == -1) //* load the file
    {
    	PCL_ERROR ("Couldn't read pcd file.\n");
    	return (-1);
    }
	sensor_msgs::PointCloud2 cloud_msg;
	toROSMsg(cloud,cloud_msg);
	cloud_msg.header.frame_id = argv[2];

	while (ros::ok())
	{
		cloud_msg.header.stamp = ros::Time::now();
		pc_pub.publish(cloud_msg);
		ros::spinOnce();
		loop_rate.sleep();
	}
	
	return 0;
}
